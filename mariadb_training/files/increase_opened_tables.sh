#!/bin/bash

export PATH=~/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/opt/thinlinc/bin



HAS_OPEN_CACHE=`grep -rin table_open_cache /etc/my*|wc -l`

if [ $HAS_OPEN_CACHE -gt 0 ];then
    echo "Not hacking the mysqld process, table_open_cache is already there"
    exit
fi


gdb -p `pidof mysqld` --batch --eval-command="set *(int*)((char *) &global_status_var+1504)+=583001">/dev/null
