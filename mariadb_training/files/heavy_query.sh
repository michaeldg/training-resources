#!/bin/bash

export PATH=~/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/opt/thinlinc/bin


if [ -e /tmp/heavyquery_pid ];
then
    kill `cat /tmp/heavyquery_pid`
fi

echo $$ >/tmp/heavyquery_pid

while [ 1 ];
do
     /usr/bin/mysql world -e "SELECT Name, Population FROM world.City WHERE CountryCode='USA' ORDER BY Population DESC LIMIT 3;">/dev/null
    sleep 1
done

