HOWTO:

- Create your inventory file 'myservers.yml' in inventories/
- Add your IP addresses in the inventory file
- Make sure you can login without interaction (passwordless SSH key, ssh-agent, etc...)
- Run this role from the root of this repository: ansible-playbook -i inventories/myservers.yml setup-training.yml